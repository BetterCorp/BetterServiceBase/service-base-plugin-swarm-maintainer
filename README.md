# Docker Swarm Maintainer service

NOTE: This service is not really a plugin but could be used as one.  
It's more designed to run in your stack to auto-update plugins in the BSB stack  


## Docker swarm config

```

version: "3.8"

services:
  swarm-maintainer:
    image: betterweb/service-base-plugin-swarm-maintainer:latest
    volumes:
      - /etc/localtime:/etc/localtime:ro
      - /var/run/docker.sock:/var/run/docker.sock
    environment:
      - BSB_LIVE="1"
    deploy:
      replicas: 1
      update_config:
        parallelism: 1
        order: stop-first
      labels:
        - "BSB_SWARM_MAINTAIN=YES"

```