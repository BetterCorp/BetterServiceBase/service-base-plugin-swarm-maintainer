import { CPlugin } from "@bettercorp/service-base/lib/ILib";
import { exec } from 'child_process';
import { DockerService, ServiceDeploymentNotification, ServiceUpdate } from '../../lib';

export class Plugin extends CPlugin<any> {
  async init(): Promise<void> {
    const self = this;
    await self.onEvent<ServiceDeploymentNotification>(null, 'image-updated', x => self.onPluginUpdated([x]));
    await self.onEvent<Array<ServiceDeploymentNotification>>(null, 'images-updated', x => self.onPluginUpdated(x));
  }

  async onPluginUpdated(data: Array<ServiceDeploymentNotification>): Promise<void> {
    try {
      for (const plugin of data)
        this.log.info(`Received update event for ${ plugin.image } v ${ plugin.version }`);

      this.log.info(`Delay for 60s`);
      for (let x = 60; x >= 0; x--) {
        this.log.info(`Delay for 60s [${ x }s]`);
        await new Promise(x => setTimeout(x, 1000));
      }
      this.log.info(`Delay for 60s - Start update process`);

      const activeServicesList = await this.execReturnableCmd<Array<DockerService>>("docker service ls --format '{{json .}}'");
      let imagesToPull: Array<string> = [];
      let serviceIdsToUpdate: Array<ServiceUpdate> = [];
      this.log.info(`Found [${ activeServicesList.length }] services active`);
      for (const service of activeServicesList) {
        const serviceImageName = service.Image.split(':')[0];
        for (const dImage of data) {
          if (serviceImageName !== dImage.image) {
            continue;
          }
          const labels = ((await this.execReturnableCmd<any>(`docker service inspect ${ service.ID }  --format '{{json .Spec.Labels}}'`, true)) || {});
          if (labels.BSB_SWARM_MAINTAIN != 'YES') {
            this.log.info(`{ service.Name }[${ service.ID }] does not have BSB_SWARM_MAINTAIN set to YES, so we will ignore it (labels)`, labels);
            continue;
          }
          const newDockerImage = `${ dImage.image }:${ dImage.version }`;
          if (service.Image === newDockerImage) {
            this.log.info(`${ service.Name }[${ service.ID }] Doesn't need an update (${ service.Image } == ${ newDockerImage })`);
            continue;
          }
          this.log.info(`${ service.Name }[${ service.ID }] Marked for update (${ service.Image } -> ${ newDockerImage })`);
          serviceIdsToUpdate.push({
            image: newDockerImage,
            id: service.ID
          });
          if (imagesToPull.indexOf(newDockerImage) < 0) {
            imagesToPull.push(newDockerImage);
            this.log.info(`${ service.ID } Marked (${ newDockerImage }) for pull image`);
          }
        }
      }
      this.log.info(`Found [${ serviceIdsToUpdate.length }] services to update to latest`);
      this.log.info(`Pulling latest images`);
      for (const img of imagesToPull) {
        await this.execCmd(`docker pull ${ img }`);
      }
      for (let service of serviceIdsToUpdate) {
        this.log.info(`Update service with ID: ${ service.id }`);
        await this.execCmd(`docker service update --force --image ${ service.image } ${ service.id }`);
      }
      this.log.info(`Update services completed`);
    } catch (xc) {
      this.log.fatal(xc);
    }
  }

  private execReturnableCmd<T = any>(cmd: string, outputsCorrectly: boolean = false): Promise<T> {
    const self = this;
    return new Promise((resolve, reject) => {
      let resultData = '';
      self.log.debug(`Run CMD (${ cmd })`);
      const script = exec(cmd);
      script.stdout!.on('data', function (data) {
        resultData += data.toString();
        self.log.debug(data.toString());
      });
      script.stderr!.on('data', function (data) {
        self.log.error(data.toString());
      });
      script.on('exit', function (code) {
        if (code == 0) {
          if (!outputsCorrectly)
            return resolve(JSON.parse(`[${ resultData.trim().split('\n').join(',') }]`));
          return resolve(JSON.parse(resultData.trim()));
        }
        self.log.error(`Command (${ cmd }) ended with code: ${ code }`);
        reject(code);
      });
    });
  }
  private execCmd(cmd: string): Promise<void> {
    const self = this;
    return new Promise((resolve, reject) => {
      self.log.info(`Run CMD (${ cmd })`);
      const script = exec(cmd);
      script.stdout!.on('data', function (data) {
        self.log.debug(data.toString());
      });
      script.stderr!.on('data', function (data) {
        self.log.error(data.toString());
      });
      script.on('exit', function (code) {
        if (code == 0) return resolve();
        self.log.error(`Command (${ cmd }) ended with code: ${ code }`);
        reject(code);
      });
    });
  }
}